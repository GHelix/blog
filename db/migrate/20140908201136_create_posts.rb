class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.string :rails
      t.string :generate
      t.string :scaffold
      t.string :post
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
